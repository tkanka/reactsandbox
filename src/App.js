import React from 'react';
import DefaultApp from './Exemples/DefaultApp'


function App() {
  return (
    <React.Fragment>
      <DefaultApp />
    </React.Fragment>
  );
}

export default App;
